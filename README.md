# Container Scanning Monorepo

### Overview

This repository demonstrates running [GitLab Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) on monorepo projects. As of July 2020, scanning each service is supported on a per service basis. There is a [feature request](https://gitlab.com/gitlab-org/gitlab/-/issues/208758) to support multiple docker file scans. The focus for this guided exploration include the following points:

- Set up a job per service to scan for vulnerabilities

## Container scan per service

Specficy [`DOCKERFILE_PATH`](https://docs.gitlab.com/ee/user/application_security/container_scanning/#available-variables) and [`CI_APPLICATION_REPOSITORY`](https://docs.gitlab.com/ee/user/application_security/container_scanning/#available-variables) to scan docker service image. 

```
# Include in root gitlab-ci.yml file
include:
  - template: Container-Scanning.gitlab-ci.yml
  - 'serviceA/.serviceA-ci.yml'

# Always ignore main scanning job. Let service pipelines manage their own scanning
container_scanning:
  rules:
    - if: $CI_JOB_ID

# Include below in service-ci.yml file
variables:
  IMAGE_TAG_SERVICE_A: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG-service-a:$CI_COMMIT_SHA

container_scan_serviceA:
  extends: container_scanning
  variables:
    DOCKERFILE_PATH: serviceA/Dockerfile
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG-service-a
  rules:
    - changes:
      - serviceA/**/*

```
